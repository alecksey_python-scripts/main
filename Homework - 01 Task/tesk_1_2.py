#Даны действительные числа x и y. Получить (|x|-|y|)/(1+|xy|)

import math
x = float(input("Введите число 'x': "))
y = float(input("Введите число 'y': "))

#Убираем ноль после запятой
def func(c):
    if c%1==0:
        return int(c)
    else:
        return c

print("(|x|-|y|)/(1+|xy|) =", func((math.fabs(x)-math.fabs(y))/(1+math.fabs(x)*math.fabs(y))))
