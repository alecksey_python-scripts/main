#Даны 2 действительных числа a и b. Получить их сумму, разность и произведение.

a = float(input("Введите число 'a': "))
b = float(input("Введите число 'b': "))

#Убираем ноль после запятой
def func(c):
    if c%1==0:
        return int(c)
    else:
        return c

print("Сумма:", func(a+b))
print("Разность:", func(a-b))
print("Произведение:", func(a*b))
